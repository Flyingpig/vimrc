set number
set hlsearch

"解决insert模式下backspace没有作用的问题
set backspace=indent,eol,start

"vim内部编码格式
set encoding=utf-8
"保存文件编码格式
set fileencoding=utf-8
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
"终端显示编码方式
set termencoding=utf-8
set ambiwidth=double "防止特殊符号无法正常显示"

"主题配置 可用颜色配置在/usr/share/vim/vim74/colors
colorscheme morning	

"启用256配色  配合256色xterm使用(export TERM=xterm-256color)
set t_Co=8 "此行在vim启动时会去读取term参数来自动设置，可不写"
set term=screen-256color "在tmux模式下，TERM会被置为默认值，需要虚拟终端告诉vim自己开启了256模式才可以，也可在.tmux.conf文件中进行配置

"启用语法高亮
syntax on

"启用特号匹配模式
set showmatch



"一键删除四个空格
set smarttab

"显示状态行
set ruler

"tab为四个空格
set tabstop=4
"设置tab长度
"set softtabstop=8
"用四个空格代替tab
set expandtab "or noexpandtab
"自动缩进为4个空格,>> or <<
set shiftwidth=4 

set autoindent
"C语言格式的自动缩进
"set cindent

"动态显示搜索结果
set is 

"文本上按下>>(增加一级缩进) <<(取消一级缩进) ==(取消全部缩进)
"set expandtab

"显示cmd
set showcmd

"高亮当前行和列
"term，可以定义其字体显示为：bold、underline、reverse、italic或standout。
"Vim识别三种不同的终端：term，黑白终端；cterm，彩色终端；gui，Gvim窗口。
"推荐使用的颜色包括：black, brown, grey, blue, green, cyan, magenta, yellow,white
set cursorline 
hi CursorLine   cterm=underline ctermbg=NONE ctermfg=NONE
set cursorcolumn
hi CursorColumn cterm=NONE ctermbg=NONE ctermfg=NONE

"取消自动备份
set nobackup

"set ex commend tab
set wildmenu    "显示菜单
set wildmode=list:longest,full     "以列表形式显示在wildmenu

" 垂直滚动时，光标距离顶部/底部的位置（单位：行）
set scrolloff=5

"文件类型自动语法高亮
auto BufRead,BufNewFile *.sv set filetype=verilog

set tags=tags;
set autochdir

call plug#begin('~/.vim/plugged') "https://github.com/junegunn/vim-plug.git
    "Plug 'https://gitlab.com/Flyingpig/vimbymyself.git'
    Plug 'preservim/nerdtree'  "目录树插件
    Plug 'junegunn/vim-easy-align'  "自动对齐
    Plug 'jiangmiao/auto-pairs' "括号添加插件
    Plug 'vim-airline/vim-airline' "底部状态栏美化 
    " 安装fonts字体 from 'https://github.com/powerline/fonts.git'
    Plug 'vim-airline/vim-airline-themes'  "状态栏美化配色
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'  }
    Plug 'junegunn/fzf.vim'
    Plug 'jreybert/vimagit'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}  "f自动补全插件
    Plug 'https://github.com/voldikss/vim-floaterm.git'
    Plug 'https://github.com/mhinz/vim-signify.git'   "git
call plug#end()


set laststatus=2  "永远显示状态栏
let g:airline_powerline_fonts = 1  " 支持 powerline 字体
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1  "显示窗口tab和buffer
let g:airline_theme='murmur'  " murmur配色不错
nmap <tab> :bn<cr> "设置tab键映射"

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_left_sep = '▶'
let g:airline_left_alt_sep = '❯'
let g:airline_right_sep = '◀'
let g:airline_right_alt_sep = '❮'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '¶'
let NERDTreeQuitOnOpen=1
" 关闭NERDTree快捷键
" map <leader>t :NERDTreeToggle<CR>
" " 显示行号
" let NERDTreeShowLineNumbers=1
" let NERDTreeAutoCenter=1
" " 是否显示隐藏文件
" let NERDTreeShowHidden=1
" " 设置宽度
" let NERDTreeWinSize=31
" " 在终端启动vim时，共享NERDTree
let g:nerdtree_tabs_open_on_console_startup=1
" " 忽略临时文件的显示
" let NERDTreeIgnore=['\.pyc','\~$','\.swp']
" " 显示书签列表
" let NERDTreeShowBookmarks=1
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

let g:coc_disable_startup_warning = 1
let NERDTreeShowHidden=1

"nerdtree
nnoremap <Leader>bl :BLines<CR>
""""""""

    let g:signify_disable_by_default = 1

    let g:signify_vcs_cmds_diffmode = { 
      \ 'git':      'git show HEAD:./%f',
      \ 'yadm':     'yadm show HEAD:./%f',
      \ 'hg':       'hg cat %f',
      \ 'svn':      'svn cat %f',
      \ 'bzr':      'bzr cat %f',
      \ 'darcs':    'darcs show contents -- %f',
      \ 'fossil':   'fossil cat %f',
      \ 'cvs':      'cvs up -p -- %f 2>%n',
      \ 'rcs':      'co -q -p %f',
      \ 'accurev':  'accurev cat %f',
      \ 'perforce': 'p4 print %f',
      \ 'tfs':      'tf view -version:W -noprompt %f',
      \ }
