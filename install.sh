#!/bin/bash

homepath=$HOME

case $1 in 
    install)
        if [ ! -d "$homepath/.vim/autoload" ]; then
            mkdir -p $homepath/.vim/autoload
            echo "mkdir $homepath/.vim/autoload"
        fi

        if [ ! -d "$homepath/.vim/plugin" ]; then
            mkdir -p $homepath/.vim/plugin
            echo "mkdir $homepath/.vim/plugin"
        fi

        if [ ! -d "$homepath/.vim/plugged" ]; then
            mkdir -p $homepaht/.vim/plugged
            echo "mkdir $homepath/.vim/plugged"
        fi

        if [ -d "$homepath/.vim/vim-plug" ]; then
            rm -rf $homepath/.vim/vim-plug
            echo "rmdir $homepath/.vim/vim-plug"
        fi

        cd $homepath/.vim
        echo "enter $homepath/.vim"
        git clone  https://github.com/junegunn/vim-plug.git
        cp -f $homepath/.vim/vim-plug/plug.vim $homepaht/.vim/autoload/
        cd -

        \cp -f .vimrc ~/.vimrc
        echo "cp .vimrc"
        ;;
    update)
        \cp -f .vimrc ~/.vimrc
        ;;
esac
