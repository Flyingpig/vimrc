## vim-plug的使用方法，

- git clone https://github.com/junegunn/vim-plug.git
- cp vim-plug/plug.vim ~/.vim/autoload/plug.vim
- 在~/.vimrc配置文件中加入下列语句

```
call plug#begin('~/.vim/plugged')   "括号内为插件的存放目录
    Plug 'https://gitlab.com/Flyingpig/vimbymyself.git'
    Plug 'preservim/nerdtree'
    "文件的话直接写文件的绝对路径即可
call plug#end()
```

## ~/.vim文件目录的说明
.   
├── autoload        此文件夹下的插件,在调用的时候才会发挥作用   
├── plugged         vim-plug管理的插件的存放位置   
├── plugin          vim每次启动时都会主动加载的插件  
└── vim-plug        vim-plug的git仓库，需要将plug.vim文件cp到~/.vim目录下  

 ### 注意，以.vim后缀的插件需要放在以上目录的下一级目录下,不得放在其二级及以下目录中，会检测不到。

## 配色说明
语法高亮的目录是  
` /usr/share/vim/vim80/syntax 在此目录下定义关键词为变量 `     
` /usr/share/vim/vim80/colors 此目录修改变量的高亮 `

## vim-airline状态栏美化插件说明

### 需要修改的配置
vim-airline-theme是vim-airline的配色插件，在使用的时候需要开启linux的终端256配色显示，操作如下：   
```
echo "export TERM=xterm-256color" > ~/.bashrc
```

